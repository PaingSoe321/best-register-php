<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="bootstrap/style.css">
</head>
<body bgcolor="lightgray">
<div class="sidenav">
 
  <button class="dropdown-btn"><i class="fa fa-apple"></i>&nbsp; &nbsp;REGISTER&nbsp;
    <i class="fa fa-plus"></i>
  </button>
  <div class="dropdown-container">
    <a href="register.php">Add</a>
    <a href="user.php">List</a>
  </div>
  </div>

<div class="main">
 <h2><i class="fa fa-home">Dashboard</i></h2>
  <hr>
</div>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

</body>
</html> 
