<?php 
include "config.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Register Form</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

<body>

  <?php
  include "admin.php";
  $mysqli=new mysqli('localhost','root','','register');
  if(isset($_GET['delete'])){
        $id=$_GET['delete'];
        $sql="DELETE FROM registerform WHERE id=$id";
        $result=mysqli_query($con,$sql);
        if($mysqli->query($sql)==true){
          header("location:user.php");
        }
      }
  $username="";
  $email="";
  $address="";
   $password="";
   $confirm_password="";
   $degree="";
   $level="";
   
  $update=false;
  
   if(isset($_GET['edit'])){
        $id=$_GET['edit'];
        $update=true;
        $sql="SELECT username,email,password,address,photo,degree,level FROM registerform WHERE id=$id";
        $result=$mysqli->query($sql)->fetch_assoc();
        $username=$result['username'];
        $email=$result['email'];
        $password=$result['password'];
        $confirm_password=$result['password'];
        $address=$result['address'];
        $photo=$result['photo'];
        $degree=$result['degree'];
        $level=$result['level'];
      }
  ?>
 <script src="JQuery.js"></script>
  <div class="edit">
  <form method="post" action="process.php" id="j-password" enctype="multipart/form-data">
   
<h1 align="center">Register Form</h1>

      
    <hr>
    <input type="hidden" name="id" value="<?php echo $id; ?>">

    <label for="username">Username:</label><br>
    <input type="text" placeholder="Enter Username" name="username" value=<?php echo $username; ?> ><br>

    <label for="email">Email:</label><br>
    <input type="email" placeholder="Enter Email" name="email" value=<?php echo $email; ?> ><br>

    <label for="psw">Password:</label><br>
    <input type="password" placeholder="Enter Password" name="password" id="password3" 
          value="<?php echo  $password; ?>" required><br>


    <label for="con-psw">Confirm Password:</label><br>
    <input type="password" placeholder="Confirm Password" name="confirm_password" id="confirm3" value=<?php echo  $confirm_password; ?> ><br>
    <p id="confirm-message3"></p>

    <label for="address">Address:</label><br>
    <input type="text" placeholder="Write Something..." name="address" value="<?php echo $address; ?>"><br>

    <label>Uploading File:</label>
    <input type="file" name="photo" required><br><br>

      <label for="" class="text-align">Degree:</label>
  <select name="degree" id="degree" class="form-control" onchange="degreeChange()">
    <option value="">Select:</option>
    <?php 
    $result=mysqli_query($con,"SELECT * FROM degree");
    while($row=mysqli_fetch_assoc($result)){ ?>
      <option value="<?php echo $row['degree_id']; ?>"> <?php echo $row['degree'] ; ?></option>
    <?php  
      }
      ?>
   </select>
<br>

      <label for="address" class="text-align">Level:</label>
  <select name="level" id="level">
    <option value="">Select:</option>
  </select>
<br>


    <?php if($update==true){?>
      <img src="<?php echo $photo; ?>" height="100px">
      <?php } ?>


    <hr>
        <br>
  
      <?php
      if($update=="true"){ ?>

     <div align="center"> 
    <button type="submit" class="registerbtn" name="update">Update</button>
</div>
<?php
      }

      else{

   ?>
     <div align="center"> 
    <button type="submit" class="registerbtn" name="login">Register</button>
</div>
   <?php
      }

      ?>
  

   
      
    </div>



</form>
</div>

<script type="text/javascript">


function degreeChange() {
var xmlhttp=new XMLHttpRequest();
xmlhttp.open("Get","data.php?degree="+document.getElementById('degree').value,false);
xmlhttp.send(null);
document.getElementById('level').innerHTML = xmlhttp.responseText;
}
  
  $(function() {
    $('#j-password [type= "password"]').keyup(function(){
      var password = $('#password3');
      var confirm = $('#confirm3');
      var message = $('#confirm-message3');

      var good_color = "#66cc66";
      var bad_color = "#ff6666";
      if(password.val()== confirm.val()){
        confirm.css('background-color',good_color);
        message.css('color',good_color).html("OK!");
        }else{
          confirm.css('background-color',bad_color);
          /*message.css('color',bad_color).html("Check Password!");*/
        }

    });
  });
</script>


</body>
</html>





